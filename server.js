const liveServer = require('live-server');
const yargs = require('yargs');
const es = require('event-stream');
const fs = require('fs');
const url = require('url');
const path = require('path');
const { Readable } = require('stream');

const yargObj = yargs
  .option('port', {
    desc: 'port to bind on',
    alias: 'p',
    type: 'number',
    default: 8080
  })
  .option('mount-path', {
    desc: 'the root URL path for requests',
    type: 'string',
    default: '/'
  })
  .option('inject', {
    desc: 'inject HTML code from the given file',
    type: 'array',
    default: []
  })
  .option('inject-var', {
    desc: 'create a variable that will be substituted in injected code (eg: myVar=myVal)',
    type: 'array',
    default: []
  })
  .option('default-index', {
    desc: 'serve this file when there is no index.html',
    type: 'string',
  })
  .positional('root', {
    desc: 'the local path from which to serve files',
    demandOption: true
  })
  .help().alias('help', 'h')

const argv = yargObj.argv;

argv.rootPath = argv._[0];

if (!argv.rootPath) {
  yargObj.showHelp();
}

function getInjectCode() {
  let injectedCode = '\n';
  for (const injectFile of argv.inject) {
    let injectCode = fs.readFileSync(injectFile).toString();
    for (const injectVar of argv.injectVar) {
      const [varName, val] = injectVar.split(/:(.+)/);
      injectCode = injectCode.replace(`{{${varName}}}`, val);
    }
    injectedCode += injectCode;
  }
  return injectedCode;
}

function inject(res, stream, injectTag) {
  console.log('Injecting custom code', injectTag, res.getHeader('content-type'));
  if (!res.getHeader('content-type').match(/^text\/html/)) return;

  const injectedCode = getInjectCode();

  // We need to modify the length given to browser
  const len = injectedCode.length + parseInt(res.getHeader('Content-Length')); 
  res.setHeader('Content-Length', len);

  if (injectTag) {
    var originalPipe = stream.pipe;
    stream.pipe = function(resp) {
      originalPipe.call(stream, es.replace(new RegExp(injectTag, "i"), injectTag + injectedCode)).pipe(resp);
    };
  } else {
    // there's no reasonable place to put the code, so just prepend it to the stream.
    const prependStream = Readable.from([injectedCode]);
    es.merge([prependStream, stream]).pipe(res);
  }
}

function middleware(req, res, next) {
  if (req.method !== "GET" && req.method !== "HEAD") return next();

  var reqpath = url.parse(req.url).pathname;
  if (reqpath !== '/') return next();

  const indexPaths = ['index.html', 'index.htm'].map(f => path.join(argv.rootPath, f));
  for (let indexPath of indexPaths) {
      if (fs.existsSync(indexPath)) return next();
  }

  // index does not exist, show help page
  if (argv['default-index']) {
    res.setHeader('Content-Type', 'text/html');
    let html = fs.readFileSync(argv['default-index']).toString();
    html = html.replace('{{injectedCode}}', getInjectCode());
    return res.end(html);
  }

  return next();
}

const opts = {
  port: argv.port,
  open: false,
  root: argv.rootPath,
  watch: [argv.rootPath],
  mount: [[argv.mountPath, argv.rootPath]],
  inject: inject,
  injectDocStart: true,
  middleware: [middleware]
}

//console.log(opts);

liveServer.start(opts);

console.log(`Started static server at http://localhost:${argv.port}${argv.mountPath}`);
