# Static Server for Codio

This is an alternative static server that can be run on codio boxes.
It uses fork of [live-server][1] which has dynamic reload capabilities.

[1]: https://github.com/BoldIdeaInc/live-server
